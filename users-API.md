**Get list of users**
----
  Returns json data (array of users' data objects).

* **URL**

  /users

* **Method:**

  `GET`
  

***
**Get single user**
----
  Returns json data about a single user.

* **URL**

  /users/:userId

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[ObjectID]`



***
**Sign up**
----
  Returns message if successful or error.

* **URL**

  /users/signup

* **Method:**

  `POST`
  
*  **Headers**

   **Required:**
   
   `Content-Type` - "application/json"
   
*  **Body params**

   **Required:**
 
   `username` - String
   `firstname` - String
   `lastname` - String
   `email` - String
   `password` - String
   
 ***
**Log in**
----
  Returns json with JWT if successful or error.

* **URL**

  /users/login

* **Method:**

  `POST`
  
*  **Headers**

   **Required:**
   
   `Content-Type` - "application/json"

*  **Body params**

   **Required:**
 
   `login` - String (username or email)
   `password` - String
   
 ***
**Update user**
----
  Returns json with info if successful or error.

* **URL**

  /users/:userId

* **Method:**

  `PUT`
*  **URL Params**

   **Required:**
 
   `id=[ObjectID]`
   
*  **Headers**

   **Required:**

   `Content-Type` - "application/json"
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
  
*  **Body params**
    *fields you want to be updated*
   `username` - String
   `firstname` - String
   `lastname` - String
   `email` - String
   `password` - String

 ***
**Delete user**
----
  Returns json with info if successful or error.

* **URL**

  /users/:userId

* **Method:**

  `DELETE`
*  **URL Params**

   **Required:**
 
   `id=[ObjectID]`
   
*  **Headers**

   **Required:**

   
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```

