#Base image 
FROM node:8.15-alpine

# Create app directory
RUN mkdir -p /src/app

# Change workdir
WORKDIR /src/app

#copy package.json
COPY package.json /src/app/package.json
COPY package-lock.json /src/app/package-lock.json

#install packages
RUN npm install

#Copy rest of the source code
COPY . /src/app

#Expose the port
EXPOSE 4000

#Start the app
CMD [ "npm", "start" ]