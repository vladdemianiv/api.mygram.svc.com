const mongoose=require ('mongoose');

const postSchema=mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    blog_id: {type: mongoose.Schema.Types.ObjectId,required:true,ref:'Blog'},
    desc: {type: String},
    image_url:{type: String ,required:true},
    created_at : {type : Date, default: Date.now}

});


module.exports= mongoose.model('Post',postSchema);