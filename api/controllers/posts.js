const mongoose= require('mongoose');
const Post = require ('../models/post');
const config=require('../../config');

exports.getAllPosts=(req,res,next)=>{
    let findParams={};
    let limit=+req.query.limit;
    let skip=+req.query.skip;
    if (req.query.blog_id){
        findParams={blog_id:req.query.blog_id}
    }
    if(req.query.search){
        findParams.desc=new RegExp(req.query.search,'i');
    }
    Post.find(findParams)
    .populate({
        path:'blog_id',
        populate:{
            path:"user_id",
            select:'_id username firstname lastname email'
        }

    })
    .sort([['created_at',-1]])
    .skip(skip)
    .limit(limit)
    .exec()
    .then(posts=>{
        console.log(posts);
        res.status(200).json(posts);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

//create new post
exports.createPost=(req,res,next)=>{
    console.log(req.file);
    const post=new Post({
        _id: new mongoose.Types.ObjectId(),
        blog_id: req.body.blog_id,
        desc: req.body.desc,
        image_url: `${config.virtualPath}/${req.file.path}`,
        
    }); 
    
    post
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message:"Post POST request handled",
                postCreated :result,
            });
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        })
};


//get single post
exports.getSinglePost=(req,res,next)=>{
    const postId=req.params.postId;
    Post.findById(postId)
        .populate({
            path:'blog_id',
            populate:{
                path:"user_id",
                select:'_id username firstname lastname email'
            }
        })
        .exec()
        .then(post=>{
            console.log(post);
            if (post){
                res.status(200).json(post);
            }
            else {
                res.status(404).json({message:"Post not found"});
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};

//update single post

exports.updatePost=(req,res,next)=>{
    const postId=req.params.postId;
    
    Post.update({_id:postId}, {$set:req.body})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });

};


//delete post
exports.deletePost=(req,res,next)=>{
    const postId=req.params.postId;

    Post.remove({_id:postId})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });

};