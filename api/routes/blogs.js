const express = require ('express');
const checkAuth=require('../middleware/check-auth');
const BlogsController=require('../controllers/blogs');

const checkPostedBlog=require('../middleware/check-posted-blog');
const checkRequestedBlog=require('../middleware/check-requested-blog');
const router = express.Router();

//get list of blogs
router.get('/',BlogsController.getAllBlogs);

//create new blog
router.post('/',checkAuth,checkPostedBlog,BlogsController.createBlog); 

router.get('/authorized',checkAuth,BlogsController.getUserBlogs);
//get single blog
router.get('/:path',BlogsController.getSingleBlog);

router.get('/id/:blogId',BlogsController.getSingleBlogById);

//update single blog
router.put('/:blogId',checkAuth,checkRequestedBlog,BlogsController.updateBlog);


//delete blog
router.delete('/:blogId',checkAuth,checkRequestedBlog,BlogsController.deleteBlog);

module.exports=router;