const jwt =require ('jsonwebtoken');
const JWT_KEY = require ('../../config').JWT_KEY;
module.exports= (req,res,next)=>{

    try {
        //header is coming in format "Bearer <token>"
        const token = req.headers.authorization.split(' ')[1];
        //verifies and returns decoded data
        const decoded = jwt.verify(token,JWT_KEY);
        req.userData=decoded;
        next();
    }
    catch(error)
    {
        return res.status(401).json({
            message: "Authentification failed"
        });
    }
}