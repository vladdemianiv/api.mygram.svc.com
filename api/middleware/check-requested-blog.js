const Blog=require('../models/blog');

module.exports= (req,res,next)=>{
    const blogId = req.params.blogId;
    Blog.findOne({ _id: blogId })
        .select("user_id")
        .exec()
        .then(blog => {
            //if updating blog is owned by logged in user
            if (blog.user_id == req.userData.userId) {
                next();
            }
            else {
                return res.status(405).json({
                    message: "You can change only your blogs"
                });
            }
        })
        .catch(err => {
            return res.status(500).json({
                error: err
            });
        });
}