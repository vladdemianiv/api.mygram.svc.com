const Blog=require('../models/blog');

module.exports= (req,res,next)=>{
    console.log(req.body);
    console.log(req.userData);
    if (req.body.user_id!=req.userData.userId){
        return res.status(405).json({
            message:"Not allowed to create other user's blog."
        });
    }
    Blog
        .findOne({path:req.body.path})
        .exec()
        .then(blog=>{
            console.log(blog);
            if (blog){
                res.status(409).json({
                    message:'Blog with such path exists',
                });
            }
            else {
                next();
            }
        })
        .catch(error=>{
            console.log(error);
            res.status(500).json({error}) ;
        });

    
}