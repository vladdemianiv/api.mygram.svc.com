const config=require('./config');

const http=require ('http');
const app=require ('./app');

const PORT = config.port;
const HOST = config.host;

const server=http.createServer(app);

server.listen(PORT,HOST);

console.log(`Running on http://${HOST}:${PORT}`);