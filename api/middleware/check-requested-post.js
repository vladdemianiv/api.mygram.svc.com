const Post=require('../models/post');
module.exports= (req,res,next)=>{

    const postId = req.params.postId;

    Post
        .findOne({_id:postId})
        .populate('blog_id')
        .exec()
        .then(post=>{
            if (post.blog_id.user_id==req.userData.userId){
                next();
            }
            else{
                return res.status(405).json({
                    message:'You can change only your posts'
                });
            }
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            });
        });
  
}