# Running service locally

### Prerequisities


In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)


### Usage

 Build docker image from Dockerfile by running command:

```shell
docker build -t api-mygram-svc-com .
```
**api-mygram-svc-com** - tagname (may be different).

Then run container from image:

```shell
docker run -d -p 3000:4000 api-mygram-svc-com
```
**-d** -  run container in background.
**-p** -  publish a container’s port(s) to the host. (you can specify another port by "-p (port):4000 " )

Now service is available at http://localhost:3000 . (or the port you specified)



To get information about containers run :

```shell
docker ps
```

To stop container run:

```shell
docker stop (container id/name)
```

To remove container run:

```shell
docker rm (container id/name)
```

name and id you can get by *docker ps*.


# Running service composed with mongodb server

## Start composition
Execute this command from project's root to run all required containers at once. `-d` stands for detached mode:
```bash
docker-compose up -d
```

## Start and rebuild images
Use `--build` option if you did some changes and want to update projects images
```bash
docker-compose up -d --build
```

## Get rid of old images
Be aware that when you rebuild image with the same name and tag you will create a new image, when an old one will become dangling.
You may identify dangling images in the list - they don't have either name or tag:
```bash
$ docker image ls
REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE
mygram-web                  1.0.0               99f3ae077f64        32 hours ago        299MB
mygram-api                  1.0.0               c058e7779568        32 hours ago        68.6MB
<none>                      <none>              315545cd9599        33 hours ago        506MB
<none>                      <none>              d94ee61db8d1        33 hours ago        70.2MB
```
Notice images with name and tag equal to `<none>`. To get rid of all dangling images use this command:
```
docker image prune
```
This will help you to save disk space on your local machine.

# Stop local composition

Execute this command from project's root to stop and remove all containers and network:
```bash
docker-compose down
```

**API service** is available at *localhost:3000*
**Mongodb server** is available at *localhost:27017*
