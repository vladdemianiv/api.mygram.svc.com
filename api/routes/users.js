const express = require ('express');
const checkAuth=require ('../middleware/check-auth');

const UsersController=require('../controllers/users');
//checks if requested user logged in
const checkRequestedUser=require('../middleware/check-requested-user');
const router = express.Router();


//get list of users
router.get('/',UsersController.getAllUsers);
//get count of users
router.get('/count',UsersController.getCount);
//
router.get('/checktoken',checkAuth,UsersController.getTokenInfo);
//get auth user
router.get('/authorized',checkAuth,UsersController.getUserByToken);
//create new user

router.post('/changepassword',checkAuth,UsersController.changePassword);

router.post('/signup',UsersController.userSignup);

//get jwt tocken & login
router.post('/login',UsersController.userLogin);

//get single user
router.get('/:idOrUsername',UsersController.getSingleUser);

//update single user
router.put('/:userId',checkAuth,checkRequestedUser,UsersController.updateSingleUser);

//delete user
router.delete('/:userId',checkAuth,checkRequestedUser,UsersController.deleteSingleUser);

module.exports=router;