**Get list of posts**
----
  Returns json data (array of posts' data objects).

* **URL**

  /posts

* **Method:**

  `GET`
  

***
**Get single post**
----
  Returns json data about a single post.

* **URL**

  /posts/:postId

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `postId=[ObjectID]`



***
**Create new post**
----
  Returns message if successful or error.

* **URL**

  /posts

* **Method:**

  `POST`

*  **Headers**

   **Required:**
 
    `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
  
*  **Body (form-data) params**
   `decs`- Text
   
   **Required:**
   
   `blog_id`- Text(Existing blog's ObjectID)
   `image` - File(`jpeg` or `png` type)

   
 ***

**Update post**
----
  Returns json with info if successful or error.

* **URL**

  /posts/:postId

* **Method:**

  `PUT`
*  **URL Params**

   **Required:**
 
   `postId=[ObjectID]`
   
*  **Headers**

   **Required:**
 
   `Content-Type` - "application/json"
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
  
*  **Body params**
    *fields you want to be updated*

    `desc`- String
    
   

 ***
**Delete post**
----
  Returns json with info if successful or error.

* **URL**

  /posts/:postId

* **Method:**

  `DELETE`
*  **URL Params**

   **Required:**
 
   `postId=[ObjectID]`
   
*  **Headers**

   **Required:**
   
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
