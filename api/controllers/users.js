const mongoose= require('mongoose');
const bcrypt =require ('bcrypt-nodejs');
const jwt = require ('jsonwebtoken');
const JWT_KEY=require ('../../config').JWT_KEY;
const User = require ('../models/user');
const ID_MATCH=/^[0-9a-fA-F]{24}$/;

exports.getCount=(req,res,next)=>{
    User.count()
        .exec()
        .then(count=>{
            res.status(200).json({
                usersCount:count
            });
        })
        .catch(error=>{
            res.status(500).json({
                error          
            });
        })
}
exports.getTokenInfo=(req,res,next)=>{
    User.findById(req.userData.userId)
        .select('_id username firstname lastname email')
        .exec()
        .then(user=>{
            res.status(200).json({
                userData:user,
            });
        })
        .catch(err=>{
            res.status(500).json({
                error:err            
            });
        });
};
exports.getUserByToken=(req,res,next)=>{
    User.findById(req.userData.userId)
        .select('_id username firstname lastname email')
        .exec()
        .then(user=>{
            res.status(200).json({
                userData:user,
            });
        })
        .catch(err=>{
            res.status(500).json({
                error:err            
            });
        });
};
exports.getAllUsers=(req,res,next)=>{
    User.find()
        .select("_id username firstname lastname email")
        .exec()
        .then(users=>{
            console.log(users);
            res.status(200).json(users);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
};

exports.getSingleUser=(req,res,next)=>{
    const idOrUsername=req.params.idOrUsername;
    let findParam;
    if(idOrUsername.match(ID_MATCH)){
        findParam={_id:idOrUsername};
    }
    else{
        findParam={username:idOrUsername};
    }
    User.findOne(findParam)
        .select("_id username firstname lastname email")
        .exec()
        .then(user=>{
            console.log(user);
            if (user){
                res.status(200).json(user);
            }
            else {
                res.status(404).json({message:"User not found"});
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};

exports.userSignup=(req,res,next)=>{
    
    User.find({
        $or:[
            {username:req.body.username},
            {email: req.body.email}
        ]
    })
    .exec()
    .then(user=>{
        if (user.length>0){
            return res.status(409).json({
                message: "Email or username is used by another person"
            });
        }
        else{
            bcrypt.hash(req.body.password,null,null,(err,hash)=>{
                
                if (err){
                    return res.status(500).json({
                        error:err
                    });
                } else {
                    const user =new User({
                        _id: new mongoose.Types.ObjectId(),
                        username: req.body.username,
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        email: req.body.email,
                        password: hash
                    });
                    user
                        .save()
                        .then(result=>{
                            res.status(201).json({
                                message:"User created"
                            });
                        })
                        .catch(err=>{
                            console.log(err);
                            res.status(500).json({
                                error:err
                            });
                        });
                    
                }
         
            });
        }
    });


   
   
};

exports.userLogin=(req,res,next)=>{
    User.findOne({
        $or:[
            {username:req.body.login},
            {email:req.body.login}
        ]
    })
    
    .exec()
    .then(user=>{
        if (!user){
            return res.status(404).json({
                message:"Invalid username or email"
            });
        }
        bcrypt.compare (req.body.password,user.password,(err,result)=>{
            if (err){
                res.status(401),json({
                    message : "Authentication failed"
                });
            }
            if (result){
                const token = jwt.sign({
                    email:user.email,
                    username:user.username,
                    userId:user._id
                },
                JWT_KEY,
                {
                    expiresIn: '1d'
                });

                const {_id,username,firstname,lastname,email}=user;
                
                res.status(200).json({
                    message :"Authentification successful",
                    token: token,
                    userData: {
                        _id,
                        username,
                        firstname,
                        lastname,
                        email

                    }
                });
            }
            else{
                res.status(401).json({
                    message: "Authentification failed. Incorrect password"
                });
            }
        });
    })
    .catch();
};
exports.changePassword=(req,res,next)=>{
    const userId=req.userData.userId;
    
   

    User.findById(userId)
        .select('password')
        .exec()
        .then(user=>{
            if (!bcrypt.compareSync(req.body.old,user.password)){
                res.status(400).json({
                    message:'Old password is incorrect',
                })
            }
            else{
                User.update({_id:userId},{$set:{password:bcrypt.hashSync(req.body.new,null)}})
                    .exec()
                    .then(result=>{
                        res.status(200).json({
                            result
                        })
                    })
                    .catch(err=>{
                        res.status(500).json({
                            error:err
                        })
                    })
            }
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            })
        })
}
exports.updateSingleUser=(req,res,next)=>{
    const userId=req.params.userId;
    if (req.body.password){
        req.body.password = bcrypt.hashSync(req.body.password, null);
        
    }
    User.update({_id:userId}, {$set:req.body})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });

    

};

exports.deleteSingleUser=(req,res,next)=>{
    const userId=req.params.userId;

    User.remove({_id:userId})
        .exec()
        .then(result=>{
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            });
        });
 

};