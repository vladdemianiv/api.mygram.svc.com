const multer=require('multer');
//multer config
const storage= multer.diskStorage({
    destination: function (req,file,cb){
        cb(null,"./uploads");
    },
    filename: function (req,file,cb){
        cb(null,new Date().getTime() + file.originalname);
    }
});
const fileFilter= (req,file,cb)=>{
    if (file.mimetype==="image/jpeg"||file.mimetype==="image/png"){
        cb(null,true);
    }
    else {
        cb(new Error("Invalid file type for image"),false);
    }
};

module.exports = multer({
    storage:storage,
    fileFilter:fileFilter,
    limits:{
        fileSize: 1024*1024*5
    }
});