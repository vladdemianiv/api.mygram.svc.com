**Get list of blogs**
----
  Returns json data (array of blogs' data objects).

* **URL**

  /blogs

* **Method:**

  `GET`
  

***
**Get single blog**
----
  Returns json data about a single blog.

* **URL**

  /blogs/:blogId

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `blogId=[ObjectID]`



***
**Create new blog**
----
  Returns message if successful or error.

* **URL**

  /blogs

* **Method:**

  `POST`

*  **Headers**

   **Required:**
 
   
   `Content-Type` - "application/json"
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
  
*  **Body params**
   `path`- String
   `theme_id` - Number

   **Required:**
   
   `user_id`- ObjectID
   `name` - String

   
 ***

**Update blog**
----
  Returns json with info if successful or error.

* **URL**

  /blogs/:blogId

* **Method:**

  `PUT`
*  **URL Params**

   **Required:**
 
   `blogId=[ObjectID]`
   
*  **Headers**

   **Required:**
 
   
   `Content-Type` - "application/json"
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
  
*  **Body params**
    *fields you want to be updated*

    `path`- String
    `theme_id` - Number
    `name` - String

 ***
**Delete blog**
----
  Returns json with info if successful or error.

* **URL**

  /blogs/:blogId

* **Method:**

  `DELETE`
*  **URL Params**

   **Required:**
 
   `blogId=[ObjectID]`
   
*  **Headers**

   **Required:**
   
   
   `Authorization` - "Bearer (JSON web token)"
   
   Example
   
     ```javascript
     const token= `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvMBearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhlbGxvQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoidmxhZG9zIiwidXNlcklkIjoiNWM3MTI2NmVjYjA2MzMwOGQwMmM4ZThjIiwiaWF0IjoxNTUwOTI0NDk5LCJleHAiOjE1NTA5MjgwOTl9.cJJVv0u6fRqzlf_1EpGByG28xpg8U4Hm7_081tdBGvM`
     
    xhr.setRequestHeader("Authorization", token);
    ```
