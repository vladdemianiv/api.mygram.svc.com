const express = require ('express');
const PostsController=require('../controllers/posts');
const checkAuth=require('../middleware/check-auth');
const checkPostedPost=require('../middleware/check-posted-post');
const checkRequestedPost=require('../middleware/check-requested-post');

const upload = require('../middleware/upload-file');



const router = express.Router();
//get list of posts
router.get('/',PostsController.getAllPosts);

//create new post
router.post('/',checkAuth, upload.single('image') ,checkPostedPost,PostsController.createPost);

//get single post
router.get('/:postId',PostsController.getSinglePost);

//update single post
router.put('/:postId',checkAuth,checkRequestedPost,PostsController.updatePost);

//delete post
router.delete('/:postId',checkAuth,checkRequestedPost,PostsController.deletePost);

module.exports=router;