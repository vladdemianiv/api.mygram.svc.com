
module.exports= (req,res,next)=>{
    console.log(req.userData);
    if (req.params.userId!=req.userData.userId){
        return res.status(405).json({
            message:"Not allowed to change another user."
        });
    }
    next();
}