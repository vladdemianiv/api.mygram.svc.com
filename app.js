'use strict';
const config=require('./config');

const express = require('express');
const bodyParser =require ('body-parser');
const mongoose=require ('mongoose');

const usersRoutes =require('./api/routes/users');
const blogsRoutes = require('./api/routes/blogs');
const postsRoutes = require ('./api/routes/posts');

//db connection
mongoose.connect(config.mongodbPath,{useNewUrlParser :true});

//request logs generator
const morgan = require ('morgan');

const app = express();

//generate logs for requests
app.use(morgan('dev'));


app.use('/uploads',express.static('uploads'));

//parsing bodies of requests
app.use (bodyParser.urlencoded({extended:false}));
app.use (bodyParser.json());

//giving access for CORS
app.use ((req,res,next)=>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers','*');

  if (req.method=='OPTIONS'){
    res.header('Access-Control-Allow-Methods', "GET, POST ,PUT ,DELETE");
    return res.status(200).json({});
  }
  next();
});

app.use('/users',usersRoutes);
app.use('/blogs',blogsRoutes);
app.use('/posts',postsRoutes)


//if request not exists
app.use ((req,res,next)=>{
  const error= new Error('Not found such request');
  error.status=404;
  next(error);
});

//handling errors
app.use ((error,req,res,next)=>{
  res.status(error.status || 500);
  res.json({
    error:{
      message: error.message
    }
  });
});


module.exports=app;
