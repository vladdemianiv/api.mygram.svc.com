const Blog=require('../models/blog');
const fs=require("fs");
module.exports= (req,res,next)=>{
    const blogId = req.body.blog_id;
    Blog.findOne({ _id: blogId }) 
        
        .exec()
        .then(blog => {
            //if updating blog is owned by logged in user
            if (blog.user_id == req.userData.userId) {
                next();
            }
            else {
                    fs.unlinkSync(req.file.path);
                return res.status(405).json({
                    message: "You can post only in your blogs"
                });
            }
        })
        .catch(err => {
            fs.unlinkSync(req.file.path);
            return res.status(500).json({
                error: err
            });
        });
}