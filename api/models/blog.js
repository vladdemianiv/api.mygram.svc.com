const mongoose=require ('mongoose');
const PATH_MATCH=/^[a-z_]+$/i;

const blogSchema=mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user_id : {type: mongoose.Schema.Types.ObjectId, required :true ,ref : "User"},
    name: {type: String , required :true},
    path: {type: String ,required:true,match:PATH_MATCH},
    theme_id: {type: Number,default:0},
    created_at : {type : Date, default: Date.now},

});


module.exports= mongoose.model('Blog',blogSchema);