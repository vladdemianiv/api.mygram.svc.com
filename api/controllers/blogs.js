
const mongoose= require('mongoose');
const Blog = require ('../models/blog');


exports.getAllBlogs=(req,res,next)=>{
    let findParams=null;
    if (req.query.user_id){
        findParams={user_id:req.query.user_id}
    }

    Blog.find(findParams)
    .populate('user_id',"_id username firstname lastname email")
    .exec()
    .then(blogs=>{
        console.log(blogs);
        res.status(200).json(blogs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

exports.getUserBlogs=(req,res,next)=>{
    
    Blog.find({user_id:req.userData.userId})
    .exec()
    .then(blogs=>{
        console.log(blogs);
        res.status(200).json(blogs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
};

//create new blog
exports.createBlog=(req,res,next)=>{

    const blog=new Blog({
        _id: new mongoose.Types.ObjectId(),
        user_id : req.body.user_id,
        name: req.body.name,
        path: req.body.path,
        theme_id: req.body.theme_id,
    }); 
    
    blog
        .save()
        .then(result=>{
            console.log(result);
            res.status(201).json({
                message:"Blog POST request handled",
                blogCreated :result,
            });
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        })
    
};

//get single blog
exports.getSingleBlogById=(req,res,next)=>{
    const blogId=req.params.blogId;
    
    Blog.findById(blogId)
        .populate('user_id',"_id username firstname lastname email")
        .exec()
        .then(blog=>{
            console.log(blog);
            if (blog){
                res.status(200).json(blog);
            }
            else {
                res.status(404).json({message:"Blog not found"});
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });
}
exports.getSingleBlog=(req,res,next)=>{
    const path=req.params.path;
    
    Blog.findOne({path})
        .populate('user_id',"_id username firstname lastname email")
        .exec()
        .then(blog=>{
            console.log(blog);
            if (blog){
                res.status(200).json(blog);
            }
            else {
                res.status(404).json({message:"Blog not found"});
            }
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({
                error:err
            })
        });

};

//update single blog
exports.updateBlog=(req,res,next)=>{
    const blogId = req.params.blogId;


    Blog.update({ _id: blogId }, { $set: req.body })
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });

};


//delete blog
exports.deleteBlog=(req,res,next)=>{
    const blogId=req.params.blogId;
    Blog.findOne({_id:blogId})
        .select("user_id")
        .exec()
        .then(blog=>{
            //if updating blog is owned by logged in user
            if (blog.user_id==req.userData.userId){
                
                Blog.remove({ _id: blogId })
                    .exec()
                    .then(result => {
                        console.log(result);
                        res.status(200).json(result);
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            }
            else{
                return res.status(405).json({
                    message:"You can delete only your blogs"
                });
            }
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            });
        });
};