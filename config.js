module.exports = {
    host : '0.0.0.0',
    port : 4000,
    virtualPath: 'http://api.mygram.svc.com',
    mongodbPath: 'mongodb://mygramdb:27017/mygram', 
    JWT_KEY: 'secret'
    
}